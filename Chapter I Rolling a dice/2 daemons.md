# Rolling a Dice and Keeping It Rolling

## Recap

As you undoubtedly recall from the previous encounter, we created the following C program to generate a random number
from 1 to 6, like a dice might have given us.

```c
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, const char **argv) {
    srand(time(NULL)); // This is just a magic to make random work. Don't think about it.
    int x = (rand() % 6) + 1;
    printf("%d", x);
}
```

We compiled this code into a native binary using `gcc -o dice dice.c` and ran it with `./dice`

## Keeping it rolling

Unfortunately, this program is only accesible to us, users of this one machine. Because this program is so useful on the one hand, but so complex that we don't want others to suffer the agonies of writing, testing and debugging it on their own machines, let's embrace the M/SOA way and make it accessible as a service, consumable through HTTP.

To achieve this intetion we'll change thigs a bit. Our script will always run in the background and write a random number into a file. We'll run a web server that will serve this file. Than, anyone will be able to throw away his/hers dices (pun inteanded) and use a modern micro-service instead. Let's edit `dice.c`

```c
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, const char **argv){
    srand(time(NULL)); // This is just a magic to make random work. Don't think about it.
    FILE* destination = fopen(argv[1], "w+");
    while (1) {
        int x = (rand() % 6) + 1;
        fprintf(destination, "%d", x);
        rewind(destination); // To over-write, instead of appending endlessly
    }
    fclose(destination);
}
```

Before running this script again, use whichever package manager relevant for your Linux distribution to install [Apache HTTPD](https://apache.org/httpd). If you're using Fedora, like I do, run `sudo dnf install -y httpd`. After you have HTTPD installed, run `systemctl enable httpd ; systemctl start httpd`. Don't warry about it too much for now, we'll get back to that shortly.

Now we can run our script `./dice` again. Notice that you don't get your terminal back. That's only to be expected and we'll get back to that. From another termial run `curl http://127.0.0.1/index.html`. Try this several times to verify your dice really is random. See? a real dice is much slower. But it's no fun at all having to use another terminal. Let's run `./dice` in the background. `./dice /var/www/html/index.html & ; curl http://localhost/index.html`. Much better. You can stop dice by running `pkill dice`

Were still quite far from perfect, though. Open two terminals; In the first terminal, run `./dice  /var/www/html/index.html &`. In the second, run `curl http://localhost/index.html` a couple of times. Now close the first terminal and run `curl` again in the second terminal a couple of times. Do you still get random numbers?

Obviously, we don't want our micro-service to stop working whenever we excidentally close the wrong terminal. The trick to do so in Linux is by doing [double fork](https://thinkiii.blogspot.com/2009/12/double-fork-to-avoid-zombie-process.html). Let's apply these changes.

```c
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>


void roll(int argc, const char **argv) {
    srand(time(NULL)); // This is just a magic to make random work. Don't think about it.
    FILE* destination = fopen(argv[1], "w");
    while (1) {
        int x = (rand() % 6) + 1;
        fprintf(destination, "%d\n", x);
        rewind(destination); // To over-write, instead of appending endlessly
    }
    fclose(destination);
}

int main(int argc, const char **argv) {
    pid_t pid; //FYI: typedef int pid_t. But is should be more readable.
    int status;

    pid = fork();
    if (pid > 0) {
        // we're in the parent process and pid1=child's process PID
        waitpid(pid, &status, 0);
    } else if (pid == 0) {
        //we're in the child process, let's do the second fork
        pid = fork();
        if (pid > 0) {
            exit(0); // so now the parent process is free to exist
        } else if (pid == 0) {
            roll(argc, argv);
        } else {
            printf("failed to do the second fork!") ;
            exit(1);
        }
    } else {
        printf("failed to do the first fork!");
        exit(1);
    }
}
```

Does it really solve our problem? Prove it to yourself. To better understand the code above, read about the library methods we've used

1. [fork](https://linux.die.net/man/3/fork)
1. [waitpid](https://linux.die.net/man/3/waitpid)

Almost without realizing we've gone the first step into [daemonizing](https://www.commandlinux.com/man-page/man7/daemon.7.html) our process. In order for it to become a true daemon, we need to detach ourselves completely from the terminal. Now you probably ask yourself; but we've already proved that we were detached from the terminal. We closed the terminal and the process kept running. To understand what we're trying to solve, change `dice.c` to `printf("the dice has spoken")` as part of the loop in `roll`.

```c
void roll(int argc, const char **argv) {
    srand(time(NULL)); // This is just a magic to make random work. Don't think about it.
    FILE* destination = fopen(argv[1], "w");
    while (1) {
        int x = (rand() % 6) + 1;
        fprintf(destination, "%d\n", x);
        printf("the dice has spoken!");
        rewind(destination); // To over-write, instead of appending endlessly
    }
    fclose(destination);
}
```

Run `./dice  /var/www/html/index.html` again. Now it's true that if you'll close the terminal the dice will keep on rolling, but as long as the terminal is open those messages will keep on bugging us. To really detach from the terminal, let's make it as follows

```c
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

void roll(int argc, const char **argv) {
  srand(time(NULL)); // Just a magic to make random work. Don't think about it.
  FILE *destination = fopen(argv[1], "w");
  while (1) {
    int x = (rand() % 6) + 1;
    fprintf(destination, "%d\n", x);
    printf("the dice has spoken!");
    rewind(destination); // To over-write, instead of appending endlessly
  }
  fclose(destination);
}

int main(int argc, const char **argv) {
  pid_t pid; // FYI: typedef int pid_t. But is should be more readable.
  int status;

  pid = fork();
  if (pid > 0) {
    // we're in the parent process and pid=child's PID
    waitpid(pid, &status, 0);
  } else if (pid == 0) {
    // we're in the child process
    signal(SIGHUP, SIG_IGN);
    signal(SIGCHLD, SIG_IGN); // so we'll ignore the grandchild
    // let's do the second fork
    pid = fork();
    if (pid > 0) {
      exit(0); // so now the parent process is free to exit
    } else if (pid == 0) {
      umask(0);
      chdir("/");
      for (int i = 0; i < sysconf(_SC_OPEN_MAX); i++) {
        close(i);
      }
      roll(argc, argv);
    } else {
      printf("failed to do the second fork!");
      exit(1);
    }
  } else {
    printf("failed to do the first fork!");
    exit(1);
  }
}
```

1. [signal](https://linux.die.net/man/2/signal)

Now try running `./dice /var/www/html/index.html` and see if we're detached enough for your liking. Once we'll run `dice.sh` it will stay in the background forever and ever. Or will it? What if the process itself dies? To manage our daemons, Linux systems usually use service managers. If you're using Fedora/CentOS/RHEL, your service manager is [SystemD](https://systemd.io). To tell it to manage `dice` as a service (which is quite different from aaS), we need to write a thing called [unit file](https://www.freedesktop.org/software/systemd/man/systemd.unit.html). Let's create a file `/usr/lib/systemd/system/dice.service`. Note that the path is imprtant, and so is the file suffix.

```ini
[Unit]
Description=Keep them dices rolling, man!
After=httpd.service

[Service]
Type=forking
Restart=always
ExecStart=/opt/dice/dice /var/www/html/index.html

[Install]
WantedBy=multi-user.target
```

To get `SystemD` to know our new service, run `systemctl daemon-reload`. Now run `systemctl start dice`. Verify it's up.

To be sure the service will start at boot time, `SystemD` needs to know in which step of the boot sequence it should run. To tell `SystemD` to run it as part of the "mutli-user" step, run `ln -s /usr/lib/systemd/system/dice.service /etc/systemd/system/multi-user.target.wants/dice.service`. Alternatively, you can run `systemctl enable dice.service`, and it'll know in which step to start the service because of the `[install]` section of the unit file. But now you know what it really does.

Now, remember this line, `Restart=always`? Try starting the service and then killing `dice` with `pkill dice` or something. Did `SystemD` succeeded in recognizing the process was killed? The reason it didn't is because `dice` is forking and it's hard for `SystemD` to follow. So let's remove all this advanced stuff we did to daemonize our script and change, in the unit file, from `Type=forking` to `Type=simple`. `SystemD` is daemonizing the process for us, but now we know what it's doing under the hood.

### Extra

1. This guide assumes we're using `SystemD`. However, many systems use `SysV`. Read about the differences between them.
