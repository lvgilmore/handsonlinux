# Processes, Threads and the CPU

## Background Matterials

For a good overview of systems calls and how they're called, see [the definitive guide to linux system calls](https://blog.packagecloud.io/eng/2016/04/05/the-definitive-guide-to-linux-system-calls/)

## Let's get to business

### Requirements

* Linux system (preferably a VM) with at least two cores
* [GCC](https://gcc.gnu.org/)

### Goal

We want to create a random number generator. This program will load at boot time and serve it's numbers over HTTP.

While doing so we'll load the CPU, learn about kernel space and user space, syscalls and other goodies.

### Steps

1. Let's start with a naive solution to generate a single random number. Let's write a simple program for that, `print_random.c`.

   ```c
   #include <stdio.h>
   #include <stdlib.h>
   #include <time.h>

   int main(int argc, const char **argv) {
      srand(time(NULL)); // This is just a magic to make random work. Don't think about it.
      int x = rand();
      printf("%d", x);
   }
   ```

   Compile it with `gcc -o print_random print_random.c`, and run it with `./print_random`.

1. Now we're wondering how will it scale. We want to expose this amazing service through a web server, and we wonder what will happen when all of the internet will request random numbers from us.

   To understand what will happen, we'll generate random numbers in a loop. Let's also drop out the print message, so we won't fill up our terminal with garbage.

   ```c
   #include <stdlib.h>
   #include <time.h>

   int main(int argc, const char **argv) {
      int x;
      while (1) {
         x = rand();
      }
   }
   ```

   Complie and run again. Now run `top`, hit `1` and see what happens. What you'll see is something like;

   ```top
   top - 11:11:11 up 1 days,  1:11,  1 user,  load average: 0.83, 0.62, 0.57
   Tasks: 300 total,   3 running, 297 sleeping,   0 stopped,   0 zombie
   %Cpu0  :  2.7 us,  0.3 sy,  0.0 ni, 96.3 id,  0.0 wa,  0.3 hi,  0.3 si,  0.0 st
   %Cpu1  :  1.0 us,  0.7 sy,  0.0 ni, 98.0 id,  0.0 wa,  0.3 hi,  0.0 si,  0.0 st
   %Cpu2  :  2.7 us,  0.3 sy,  0.0 ni, 97.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
   %Cpu3  :100.0 us,  0.0 sy,  0.0 ni,  0.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
   MiB Mem :  15885.9 total,   3266.1 free,   5950.0 used,   6669.8 buff/cache
   MiB Swap:   8012.0 total,   7933.1 free,     78.9 used.   8915.6 avail Mem

       PID USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND
    531838 xxuser    20   0    2188    764    696 R 100.0   0.0   0:11.36 print_random
    129535 xxuser    20   0 2744116 259328  35408 S   5.7   1.6  40:46.28 task1
      2983 xxuser    20   0  931772 337400  26660 R   4.3   2.1  32:19.58 task2
   ```

   What you'll notice is that one core, `Cpu3`, is loaded at full capacity while the others are resting. How can we tell? `id` stands for idle.

1. Let's try and spread the load more evenly

   ```c
   #include <stdlib.h>
   #include <time.h>

   int main(int argc, const char **argv) {
      int x;
      int pid = fork();
      while (1) {
         x = rand();
      }
   }
   ```

   We'll run it again and check the output of `top`

   ```top
   top - 11:11:11 up 1 days,  1:11,  1 user,  load average: 0.83, 0.62, 0.57
   Tasks: 300 total,   3 running, 297 sleeping,   0 stopped,   0 zombie
   %Cpu0  :  2.7 us,  0.3 sy,  0.0 ni, 96.3 id,  0.0 wa,  0.3 hi,  0.3 si,  0.0 st
   %Cpu1  :100.0 us,  0.0 sy,  0.0 ni,  0.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
   %Cpu2  :  3.7 us,  1.0 sy,  0.0 ni, 95.0 id,  0.0 wa,  0.3 hi,  0.0 si,  0.0 st
   %Cpu3  :100.0 us,  0.0 sy,  0.0 ni,  0.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
   MiB Mem :  15885.9 total,   3266.1 free,   5950.0 used,   6669.8 buff/cache
   MiB Swap:   8012.0 total,   7933.1 free,     78.9 used.   8915.6 avail Mem

       PID USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND
    531848 xxuser    20   0    2188    764    696 R 100.0   0.0   0:11.36 print_random
    531849 xxuser    20   0    2188    764    696 R 100.0   0.0   0:11.36 print_random
    129535 xxuser    20   0 2744116 259328  35408 S   5.7   1.6  40:46.28 task1
      2983 xxuser    20   0  931772 337400  26660 R   4.3   2.1  32:19.58 task2
   ```

   * How many processes will be spawned if we'll `fork()` twice? Three times? etc.

1. 